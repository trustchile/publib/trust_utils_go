module gitlab.com/trustchile/publib/trust_utils_go

go 1.16

require (
	github.com/joho/godotenv v1.3.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	go.uber.org/zap v1.17.0
)
