# README

## What is Trust utils

Trust utils is a Go package designed to give support to new Go project who should interact with different Trust Company's services to validate their operation with actions like log events, authenticate credential or append custom headers key/values.

## Use on Go Project

### Get the trust utils package

To retrieve the utils package and add it to a different project, in a terminal on the local project folder execute the _go get_ command:

```shell
  go get gitlab.com/trustchile/publib/trust_utils_go
```

Or, in your _go.mod_ file add the repository link with the package version to use on the require list:

```text
  module example.com/test

  go 1.16

  require(
    ...
    gitlab.com/trustchile/publib/trust_utils_go v0.1.3
    ...
  )
```

Then use the command _go mod tidy_ on the terminal.

### Use Trust Utils package

#### Use of regular function's package

To use the package in your code, import it as a regular package:

```go
  package main
  
  import (
    ...
    "gitlab.com/trustchile/publib/trust_utils_go/pkg/tokengen"
    ...
  )
```

Then call the function in your function as a regular package:

```go
  func main(){
    // Some init code
    accessToken := tokengen.GetAccessToken(contextStructure)
    // Some use to the token
    // ...
    // End of the function
  }
```

#### Use of middleware function's package

Trust utils also implements middleware functions to personalize your request. To use them first import the package as shown before:

```go
  package main
  
  import (
    ...
    "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustmiddleware"
    ...
  )
```

Then, in the route declaration, incorporate the middleware function, wrapping more the handler function for the requests:

```go
  func main(){
    // Some init code
    mux := http.NewServeMux()
    mux.Handle("/", trustmiddleware.AuthCheck((myHandler)))
    // More routes
    // ...
    // Server init
    err := http.ListenAndServe(":3000", mux)
    log.Fatal(err)
  }

  func myHandler(w http.ResponseWriter, r *http.Request){
    // Handler for "/" route
  }
```

 The previous example shows the syntax when using the standart router provides by Go. But if advances packages for routing, such as chi, gorilla-mux, alice are use, simply use their rules for middleware managing.

 ```go
  // Chi router example
  func main(){
    // Some init code
    r := chi.NewRouter()
    r.Group(func(r chi.Router) {
      r.Use(trustmiddleware.AuthCheck)
      r.Get("/", myHandler)
    })
    // More routes
    // ...
    // Server init
    err := http.ListenAndServe(":3000", r)
    log.Fatal(err)
  }

  func myHandler(w http.ResponseWriter, r *http.Request){
    // Handler for "/" route
  }
```

#### Packages description

* **eventlog** : Package used to report service's information  to the Trust Events' Service.
* **scopecheck** : Package provider of scopes and permission verification. Need the use of AuthCheck middleware.
* **tokengen** : Package used to retrieve an access token from Trust Oauth Provider's Service.
* **tracehelper** : Package provider of function to get and provide information of custom headers in the requests headers. To use the setter methods assure to use the getter or TraceCapture middleware first.
* **trustutils** : Package with multiple kind of functions used to support the rest of the module. Except for specific functions, is not recommended use them directly. The exception are:
  * _Init Function_ : Function that initialize internal package's structures used in the module. THIS FUNCTION MUST BE CALLED AT THE START OF THE MAIN FUNCTION.
  * _CleanHttpUrl_ : Function that adapts url strings to the syntax _http://..._
  * _NeedVerification_ : Function used to determine if the redacted service should be verified with the Trust's External Services.
* **trustmiddleware** : Package that contains all the middleware function present in this module. The list of these middleware is:
  * _AuthCheck_ : Middleware used to determine if the access token is permitted to use in the Trust's External Oauth service, and retrieve the data related to that token.
  * _TraceCapture_ : Middleware used to retrieve the custom headers key/value used by Trust Service, if present.

## Version History

### 25/06/2021 / v0.1.4

* Updated Readme with instructions for the use of this package.

### 24/06/2021 / v0.1.3

* Corrected misaligned pointer when retrieved data from cache in AuthCheck.

### 24/06/2021 / v0.1.2

* Updated error messages on the files AuthCheck, TokenGen and EventLog.

### 24/06/2021 / v0.1.1

* Updated message indicating absence of .env file in trustutils Init()

* Repair middleware TraceCapture to capture trace header in different capitalization.

### 23/06/2021 / v0.1.0

* First version of the package.
