// Package trustutils contains methods to initializate all the needed packages
// to start the use the trust middleware and to add various support
// functionalities to the trust middleware
//
// The utils file contain multiple support functions from interact with request
// contexts to check different environment variables. Because multiple of these
// function are used and wrapped by another package (like scopecheck), is not
// recommended to use them directly, with exception of the functions
// CleanHttpUrl, NeedVerification.
//
// It is recommended to use "trust" or "utils" as import name of the package
// ex: trust/utils "example.com/trust-package-go/pkg/trustutils"
package trustutils

import (
	"context"
	"os"
	"strconv"
	"strings"
)

// Structure containing all the data related to an access token provided by the
// Trust Oauth Provider, used to manipulate that data between different requests
// context
type TokenInfo struct {
	ResourceOwnerID  string
	Scopes           []string
	ClientID         string
	ClientCompanyUID string
	CompanyUID       string
	SystemicToken    bool
}

// Structure containing all the data related to Resource Owner of a service,
// used to manipulate that data between different requests
// context
type ResourceOwner struct {
	Id string
}

// New type define to use as key for the context structures
type key string

// Constants values used as key in the context structure to store token info and
// resource owner
const TInfoKey key = "token_info"
const ROwnerKey key = "resource_owner"

// Function used to adapt the url to the expected syntax. Receive as argument
// the unformatted string and return the formatted url
func CleanHttpUrl(url string) string {

	// First check if has the prefix http, if not, add it
	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
	}

	// Then, remove all the whitespace and invalid characters to return it
	return strings.TrimSpace(url)
}

// Simple function used to retrieve the company uid from a context. Expect as
// argument the request context and return the company uid if found or empty
// string if not.
func GetTokenCompanyUID(ctx context.Context) string {

	// Create a variable with the failure value as default
	companyUID := ""

	// Then try to get the value from the context, executing safe casting and
	// trying to avoid mistaken values
	if ti := ctx.Value(TInfoKey); ti != nil {
		if tInfo, ok := ti.(TokenInfo); ok {
			companyUID = tInfo.CompanyUID
		}
	}

	// Then, return the value retrieved or the default failure value
	return companyUID
}

// Simple function used to retrieve the token info from a context. Expect as
// argument the request context and return a pointer to TokenInfo if found or
// nil if not.
func GetTokenInfo(ctx context.Context) *TokenInfo {

	// Create a variable with the failure value as default
	var tokenInfo *TokenInfo = nil

	// Then try to get the value from the context, executing safe casting and
	// trying to avoid mistaken values
	if ti := ctx.Value(TInfoKey); ti != nil {
		if tInfo, ok := ti.(TokenInfo); ok {
			tokenInfo = &tInfo
		}
	}

	// Then, return the value retrieved or the default failure value
	return tokenInfo
}

// Simple function used to retrieve the token infos scopes from a context.
// Expect as argument the request context and return the scopes if found or
// nil if not.
func GetTokenScopes(ctx context.Context) []string {

	// Create a variable with the failure value as default
	var scopes []string = nil

	// Then try to get the value from the context, executing safe casting and
	// trying to avoid mistaken values
	if ti := ctx.Value(TInfoKey); ti != nil {
		if tInfo, ok := ti.(TokenInfo); ok {
			scopes = tInfo.Scopes
		}
	}

	// Then, return the value retrieved or the default failure value
	return scopes
}

// Function that determines if a access token corresponds to an admin token.
// Expect as argument the request context to get token info and return true if
// correspond to a admin token or false if is not the case.
func IsAdminToken(ctx context.Context) bool {

	// Obtain the companyUID from the token
	TokenCompanyUID := GetTokenCompanyUID(ctx)

	// And a token is considered admin when the string is 0 or admin
	return TokenCompanyUID == "0" || TokenCompanyUID == "admin"
}

// Function that determine if a service must be verificate using environment
// variables. Return true when the service need verification and false in the
// opposite case.
func NeedVerification() bool {

	// First, get the value of the application environment
	appENV := strings.ToLower(os.Getenv("APP_ENV"))

	// Then, check if exist the env variable FORCE_AUTHENTICATION and convert it
	// to bool
	forceAuthStr := strings.ToLower(os.Getenv("FORCE_AUTHENTICATION"))

	// Parse bool return true when the string is 1, t, T, TRUE, true or True and
	// false in any other case
	forceAuth, _ := strconv.ParseBool(forceAuthStr)

	// Then, a service need verification when the app environment is production or
	// when the env variable Force authentication is true
	return (appENV == "production") || (forceAuth)
}
