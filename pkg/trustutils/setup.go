// Package trustutils contains methods to initializate all the needed packages
// to start the use the trust middleware and to add various support
// functionalities to the trust middleware
//
// The setup file contain the required function Init that start all the internal
// packages used by the trust middleware. BE SURE TO EXECUTE INIT AT THE START
// OF YOUR MAIN FUNCTION OF THE PROJECT
//
// It is recommended to use "trust" or "utils" as import name of the package
// ex: trust/utils "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustutils"
package trustutils

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/trustchile/publib/trust_utils_go/internal/cache"
	"gitlab.com/trustchile/publib/trust_utils_go/internal/logger"
)

// Function that load the environmental variables from an env file, initialize
// the logger and initialice the cache with a defaultTime of 5 minutes and a
// expirationTime of 10 minutes. THIS FUNCTION MUST BE CALLED AT THE START FROM
// THE MAIN FUNCTION OF THE PROJECT
func Init() {

	// First start loading the env file variables
	err := godotenv.Load()

	// If loading file fail, print message and try to set application environment
	// development as
	if err != nil {
		fmt.Println("[Info] trustutils/setup.go:30, msg: .env file not found")
		if os.Getenv("APP_ENV") == "" {
			_ = os.Setenv("APP_ENV", "development")
		}
	}

	// Then initialize the logger
	logger.CreateLoggerInstance()
	logger.Writter.Info("Logger Writer ready to work.")

	// Then the cache starts
	cache.CreateCacheInstance(5*time.Minute, 10*time.Minute)

	//Finally, print a message with the environment
	logger.Writter.Infof("Running in %v environment", os.Getenv("APP_ENV"))
}
