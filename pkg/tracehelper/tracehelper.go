// Package tracehelper implements differents functions to access or incorporate
// trace headers to different structures.
//
// It is recommended to use "trace" as import name of the package
// ex: trace "example.com/trust-package-go/pkg/tracehelper"
package tracehelper

import (
	"context"
	"net/http"
)

// New type define to use as key for the context structures
type tKey string

// New type hash alike to store the trace headers in the context structures
type HeaderMap map[string][]string

// Constant value used as key in the context structure to store trace headers
const THeaderKey tKey = "trace_headers"

// Function that generate a new header map containing the trace headers and its
// values. Uses as argument the context of the request and an initial headerMap.
// Return the resultant headerMap
func AddOnHeaderMap(ctx context.Context, hM HeaderMap) HeaderMap {

	// First recover the trace header from the context structure storage
	hMap := GetHeaders(ctx)

	// If are not trace header present in the context, return the headerMap
	// passed as argument
	if hMap == nil {
		return hM
	}

	// If are trace header present, make a copy of the argument headerMap.
	// Because the header Map structure does not contain pointers, a simple copy
	// is enough
	newHM := hM

	// Go over the recovered header Map and incoporate the trace headers values to
	// the new header map
	for key, element := range hMap {
		newHM[key] = element
	}

	// Then return the new header map
	return newHM
}

// Wrapper function that update a http header to contain the trace headers
// present in the context structure using AddOnHeaderMap function. Uses as
// argument the context of the request and the header to edit
func AddOnHTTPHeader(ctx context.Context, h http.Header) {

	// Obtain a headerMap using the request context and a empty headerMap
	traceHeader := AddOnHeaderMap(ctx, make(HeaderMap))

	// Go over the header map and incorporate the key and values to the header
	for key, value := range traceHeader {
		h[key] = value
	}
}

// Function that return the list of key used in the trace headers. This is
// the idiomatic way to generate a constant array of string
func GetHeaderKeys() []string {
	return []string{
		"x-request-id",
		"x-b3-traceid",
		"x-b3-spanid",
		"x-b3-parentspanid",
		"x-b3-sampled",
		"x-b3-flags",
		"x-ot-span-context",
	}
}

// Function used to recover the header Map containing the trace headers key and
// values. Use as argument the context structure from where to obtain the header
// map and return the retrieved map
func GetHeaders(ctx context.Context) HeaderMap {

	// Using safe casting and the standart function of the context structure, try
	// to retrieve the header map
	if th := ctx.Value(THeaderKey); th != nil {
		if hMap, ok := th.(HeaderMap); ok {

			// If recovered, return it to the caller
			return hMap
		}
	}

	// If not, return a nil object
	return nil

}
