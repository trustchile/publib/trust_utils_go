//Package eventog provides a function that allow to send information about a
//service or request to the trust event service
package eventlog

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/trustchile/publib/trust_utils_go/internal/logger"
	slice "gitlab.com/trustchile/publib/trust_utils_go/internal/slicehelper"
	"gitlab.com/trustchile/publib/trust_utils_go/pkg/tokengen"
	trace "gitlab.com/trustchile/publib/trust_utils_go/pkg/tracehelper"
	utils "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustutils"
)

// Structure that contain all the relevant data to the trust event service
type LogData struct {
	// UserID: Id of the user who make the operation, is set automatically but can
	// be override and can be null
	UserID string `json:"user_id"`
	// CompanyID: Id of company associated to the resource, is set automatically
	// but can be override
	CompanyID string `json:"company_id"`
	// Resource: name of the resource involved in the operation, must be in upper
	// case and singular, example: 'DEVICE'
	Resource string `json:"resource"`
	// Operation: the operation made to the resource, can be ['CREATE', 'UPDATE',
	// 'READ', 'DELETE']
	Operation string `json:"operation"`
	// Result: if the operation is success or not
	Result bool `json:"result"`
	// Relevance: define the importance of this event, can be ['INFO',
	// 'IMPORTANT', 'ERROR']
	Relevance string `json:"relevance"`
	// ResourceID: id of the resource
	ResourceID string `json:"resource_id"`
	// ResourceID2: aux id of the resource, prefer use resource_id
	ResourceID2 string `json:"resource_id_2"`
	// Description: a small text with the description of the event
	Description string `json:"description"`
	// AuditID: Uuid of the audits associated to this event, only if apply
	AuditID string `json:"audit_id"`
	// Service: Name of the resource
	Service string `json:"service"`
}

// Function that initialize a request to send relevant data to the trust event
// service. Receive as argument the context of the request where this method is
// called and a LogData structure containing the data to send. This function
// return true if the request if completed successfully and false if fails.
func SendLogToService(ctx context.Context, lData LogData) bool {

	// First check if the EVENTS_URL variable is set. If not, stop the function.
	eventURL := os.Getenv("EVENTS_URL")
	if eventURL == "" {
		logger.Writter.Error("EVENTS_URL environment variable is not defined, so event is not send")
		return false
	}

	// Using a complementary function, check if the data to deliver is valid. If
	// not, end the function execution.
	if result, err := lData.isValidEventData(); !result {
		logger.Writter.Errorw("Error while validating event data", "error", err)
		return false
	}

	// Try to get the proyect name from a environmental variable
	lData.Service = os.Getenv("PROJECT_NAME")
	if lData.Service == "" {
		logger.Writter.Error("PROJECT_NAME environment variable is not defined")
		return false
	}

	// And try to get the userID from the context of the request
	if ctxData := ctx.Value(utils.ROwnerKey); ctxData != nil {
		if rOwner, ok := ctxData.(utils.ResourceOwner); ok {
			lData.UserID = rOwner.Id
		} else {
			logger.Writter.Debug("Could not get Resource Owner ID from Request Context")
		}
	}

	// After all the verifications, execute the request to the trust event service
	if err := eventRequest(ctx, lData, eventURL); err != nil {

		// If a error is returned, the request could not be completed/the response
		// from the server is not the expected, so the function ends returning false
		logger.Writter.Errorw("Error while requesting to Event Service", "error", err)
		return false
	}

	// If everything is ok, return true
	return true
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// PRIVATE FUNCTIONS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Function in charge of send a request to the trust event service, taking the
// information of a LogData structure and the context of a request, and the url
// for the event service. If the process does not complete or the response from
// the service is not the expected, return a related to the case error
func eventRequest(ctx context.Context, lData LogData, eventURL string) error {

	// First, set up the structures to use to make the request
	url := utils.CleanHttpUrl(eventURL + "/v1/event/service")

	header := &http.Header{}
	client := &http.Client{}
	method := "POST"

	// After that, try to convert the log data to json
	payload, err := json.Marshal(lData)
	if err != nil {
		return fmt.Errorf("problem while marshal payload: %v", err)

	}

	// With the jsoninfy data, the url and the method, build the request to send
	request, err := http.NewRequest(method, url, bytes.NewBuffer(payload))
	if err != nil {
		return fmt.Errorf("problem creating connection to Events Service: %v", err)
	}

	// Then, set the required header data, including the authorization, the
	// content type and the different trace header used by trust
	header.Set("Content-Type", "application/json")
	header.Set("Authorization", tokengen.GetAccessToken(ctx))
	trace.AddOnHTTPHeader(ctx, *header)
	request.Header = *header

	// Then execute the request
	response, err := client.Do(request)
	if err != nil {
		return fmt.Errorf("cant connect to Events Service: %v", err)
	}
	defer response.Body.Close()

	// If the requests response is not 201, return an error using the status code
	// of the response and his body
	if response.StatusCode != http.StatusCreated {
		error_string := "Events Service Error Response - " +
			"Status: " + response.Status + ". Body: "
		body, err := json.Marshal(response.Body)
		if err != nil {
			error_string += fmt.Sprintf("Problem while marshal response body - %v", err)
		} else {
			error_string += string(body)
		}

		logger.Writter.Info(error_string)
		return fmt.Errorf(strings.ToLower(error_string))
	}

	// If the requests response is 201, return nil to represent the absence of
	// errors
	return nil
}

// Function for LogData structures that verifies different information:
// - First, check if the service need verification
// - Second, that the data stored includes the operation of the request
// - Last, check if the structure contain the relevance of the log
// If all of these check are successful, the method return true, if any of these
// fails, return false and an error indicating the context of the failure
func (lData LogData) isValidEventData() (bool, error) {

	// Check environment variables to determine if the request need a verification
	if !utils.NeedVerification() {
		return false,
			fmt.Errorf("service doesnt need send event")
	}

	// Then check if the structure contains a valid operation
	if !slice.HasValue(operationList(), lData.Operation) {
		return false,
			fmt.Errorf(lData.Operation + " is not a valid event operation," +
				" the valid values are: " + strings.Join(operationList()[:], ","))
	}

	// Finally check if the structure contains the relevance of the request
	if !slice.HasValue(relevanceList(), lData.Relevance) {
		return false,
			fmt.Errorf(lData.Relevance + " is not a valid event relevance," +
				" the valid values are: " + strings.Join(relevanceList()[:], ","))
	}

	// If all the verification are ok, return true
	return true, nil
}

// Function that return a array of strings containing all the supported
// operations for the trust event service. This is the idiomatic way to generate
// a constant array of string
func operationList() []string {
	return []string{"CREATE", "DELETE", "READ", "UPDATE"}
}

// Function that return a array of strings containing all the supported
// revelance for the trust event service. This is the idiomatic way to generate
// a constant array of string
func relevanceList() []string {
	return []string{"ERROR", "IMPORTANT", "INFO"}
}
