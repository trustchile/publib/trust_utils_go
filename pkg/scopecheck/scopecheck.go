// Package scopecheck implements differents functions for a credential to check
// permissions to access data, check the scope of the given access token or
// all the information at once
//
// It is recommended to use "scope" as import name of the package
// ex: scope "gitlab.com/trustchile/publib/trust_utils_go/pkg/scopecheck"
package scopecheck

import (
	"context"
	"strings"

	"gitlab.com/trustchile/publib/trust_utils_go/internal/logger"
	slice "gitlab.com/trustchile/publib/trust_utils_go/internal/slicehelper"
	utils "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustutils"
)

// Options is a struct that store all the variants that can affect the
// verification made by this package
type Options struct {
	// Partial Match is a boolean that allow to the scopes to be check be
	// contained in the token scopes, instead of be exact the same, when the
	// method containsScope is called
	PartialMatch bool
}

// Function that check if the credential have permissions for access to data
// from the specific company. If the credentials are a systemic token, all
// the permissions are granted. The permission are valid when the function
// returns true.
// The data of company to access are indicated in the string companyUID, the
// credential information are contained in the context of the request

func CheckCompanyPermissions(ctx context.Context, companyUID string) bool {

	// If the credentials doesnt need verification (determined when checking
	// environmental variable), return true immediately
	if !utils.NeedVerification() {
		return true
	}

	// Check if is systemic token (token company UID equals to 0)
	var integrationToken, systemicToken bool
	tokenCompanyUID := utils.GetTokenCompanyUID(ctx)
	systemicToken = (tokenCompanyUID == "0")

	// Or if is a user access token / integration token
	if len(companyUID) > 0 {
		integrationToken = (tokenCompanyUID == companyUID)
	} else {
		integrationToken = false
	}

	// If the credential corresponds to some of the previous case, return true
	if systemicToken || integrationToken {
		return true
	}

	logger.Writter.Info("Invalid permissions for the request credentials")
	return false
}

// Function that receive a slice of strings and check if that slice match with
// slice present in the access token scopes field. If the verification pass,
// return true. The slice to check is scopes, the access token to compare is
// contained in the context of the request and the options structure is used
// to check if partial_match is present.
func CheckScope(ctx context.Context, scopes []string, opts Options) bool {

	// Skip verification if is not required
	if !utils.NeedVerification() {
		return true
	}

	// Try to get the token information from the context. If are not token info
	// avalaible, return false
	if utils.GetTokenInfo(ctx) == nil {
		logger.Writter.Info("Unable to check scopes because token_info has no" +
			" data, check if AuthorizationCheck middleware is used in your controller")
		return false
	}

	// If the token info is present, get the token scopes and compare it with
	// the token to test. If the comparison are correct, return true
	if containsScope(utils.GetTokenScopes(ctx), scopes, opts.PartialMatch) {

		return true

	} else {

		// If the comparison fails, return false
		msg := "Invalid scopes for the request credentials, required scopes: " +
			strings.Join(scopes[:], ",") + ", but the scopes in the request are: " +
			strings.Join(utils.GetTokenScopes(ctx)[:], ",")
		logger.Writter.Info(msg)

		return false
	}
}

// This function is a centralized method to validate scopes and company data
// access, the opts parameter receive the same values as CheckScope and
// CheckCompanyPermissions combined
func PermissionsCheck(ctx context.Context, companyUID string, scopes []string, opts Options) bool {

	return CheckScope(ctx, scopes, opts) && CheckCompanyPermissions(ctx, companyUID)
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// PRIVATE FUNCTIONS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Function that takes two slices: tokenScopes and scopes, and compare its
// contents. If partialMatch is true, check if scopes is contained in
// tokenScopes. If false, scopes and tokenScopes contents must be the same
func containsScope(tokenScopes []string, scopes []string, partialMatch bool) bool {

	// If partial match is true
	if partialMatch {

		// The number of elements of tokenScope must be bigger that the difference
		// of tokenScopes and scopes
		tokenScopeCount := len(tokenScopes)
		diffScopeCount := len(slice.Substract(tokenScopes, scopes))
		return diffScopeCount < tokenScopeCount

	} else {

		// If partial match is false, the element of scopes and tokenScopes must be
		// the same, then, the numbers of elements of the difference must be 0
		return len(slice.Substract(scopes, tokenScopes)) == 0
	}
}
