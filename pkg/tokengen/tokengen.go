// Package scopecheck implements differents functions for a credential to check
// permissions to access data, check the scope of the given access token or
// all the information at once
package tokengen

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/trustchile/publib/trust_utils_go/internal/cache"
	"gitlab.com/trustchile/publib/trust_utils_go/internal/logger"
	trace "gitlab.com/trustchile/publib/trust_utils_go/pkg/tracehelper"
	utils "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustutils"
)

// Structure used to tidy and store the response from the oauth service when
// the package ask for an access token
type getTokenResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	Scope       string `json:"scope"`
	CreatedAt   int    `json:"created_at"`
}

// Structure used to store the data required from the oauth service to deliver
// a new access token
type credential struct {
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
}

// Function used to retrieve an access token. If exist an internal token stored
// in cache, it retrieved from there, if not, request a new one from the oauth
// service. This function return a string with the access token retrieved,
// or a empty string. As an argument, the context of the request is needed
func GetAccessToken(ctx context.Context) string {

	// First check if the required env variables are present
	providerURL := os.Getenv("OAUTH_PROVIDER_URL")
	clientID := os.Getenv("OAUTH_CLIENT_ID")
	clientSecret := os.Getenv("OAUTH_CLIENT_SECRET")

	if len(providerURL) == 0 {
		logger.Writter.Info("OAUTH_PROVIDER_URL ENV is missing. " +
			"Check if .env file is loaded")
		return "" //empty access token
	}

	if len(clientID) == 0 || len(clientSecret) == 0 {
		logger.Writter.Info("OAUTH_CLIENT_ID or OAUTH_CLIENT_SECRET ENV is " +
			"missing, cannot obtain a new access token")
		return "" //empty access token
	}

	// Then try to read from cache for an internal token
	var token string
	x, found := cache.Data.Get("internal_access_token")
	if found != nil {
		token = ""
	} else {
		err := json.Unmarshal(x, &token)
		if err != nil {
			token = ""
		}
	}

	// If a token is obtained from cache, return the token
	if len(token) > 0 {
		logger.Writter.Info("Get new access token from cache")
		return "Bearer " + token
	}

	// If a token is not in cache, generate a new credential structure to store
	// the required data for the oauth service
	clientCredential := credential{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		GrantType:    "client_credentials",
	}

	// Then call the method tokenRequest, to realice the http request to the
	// provider server
	token, err := tokenRequest(ctx, providerURL, clientCredential)
	if err != nil {
		logger.Writter.Errorw("Problem while requesting a new token to the "+
			"oauth service", "error", err)
	}

	// Finally, return the result of the tokenRequest
	return token
}

// Method used to efectuate a request to the oauth service to get a new
// access token. As argument, this function need the context of the parent
// request, the url of the provider service and the credential of the client.
// As result, the method return a string with the token obtained or a empty
// string and a descriptive error
func tokenRequest(ctx context.Context, providerURL string, clientCredential credential) (string, error) {

	// First, set up the structures to use to make the request
	url := utils.CleanHttpUrl(providerURL + "/oauth/token")

	method := "POST"
	client := &http.Client{}
	header := &http.Header{}

	// After that, try to convert the client credential data to json
	payload, err := json.Marshal(clientCredential)
	if err != nil {
		result_error := fmt.Errorf("problem while marshal payload: %v", err)
		return "", result_error
	}

	// With the jsoninfy data, the url and the method, build the request to send
	request, err := http.NewRequest(method, url, bytes.NewBuffer(payload))
	if err != nil {
		result_error := fmt.Errorf("problem creating connection to "+
			"Oauth Provider: %v", err)
		return "", result_error
	}

	// Then, set the required header data, like the content type
	// and the different trace header used by trust
	header.Set("Content-Type", "application/json")
	trace.AddOnHTTPHeader(ctx, *header)
	request.Header = *header

	// Then execute the request
	logger.Writter.Info("Get new access token on " + url)
	response, err := client.Do(request)
	if err != nil {
		result_error := fmt.Errorf("cant connect to Oauth Provider: %v", err)
		return "", result_error
	}
	defer response.Body.Close()

	// Try to decode the body of the response in a get token Response structure
	tokenResponse := &getTokenResponse{}
	err = json.NewDecoder(response.Body).Decode(tokenResponse)
	if err != nil {
		result_error := fmt.Errorf("cant parse Oauth Provider Response: %v", err)
		return "", result_error
	}

	// If the response is a status ok, get the access token, store it in the cache
	// and return it to the caller of the function
	if response.StatusCode == http.StatusOK {

		// store the access token in cache to the expire date
		// add 5 minutes less of drift to access token cache
		expiration := time.Duration(tokenResponse.ExpiresIn-60*5) * time.Second
		cache.Data.Set("internal_access_token", tokenResponse.AccessToken, expiration)

		return "Bearer " + tokenResponse.AccessToken, nil

	} else {

		// If the requests response is not 200, return an error using the status
		// code of the response and his body
		error_string := "Oauth Provider Server Error Response - " +
			"Status: " + response.Status + ". Body: "

		body, err := json.Marshal(tokenResponse)
		if err != nil {
			error_string += fmt.Sprintf("Problem while marshal response body - %v", err)
		} else {
			error_string += string(body)
		}
		logger.Writter.Info(error_string)
		return "", fmt.Errorf(strings.ToLower(error_string))
	}
}
