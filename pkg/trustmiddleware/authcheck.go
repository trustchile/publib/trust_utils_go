// Package trustmiddleware implements, as the name says, differents middleware
// related to the trust services to adapt the different request associated to
// the services.
//
// The file authcheck implements the authentification verification middleware,
// that used to verify with the trust oauth service if the bearer access token
// is in the trust environment and used to retrieve the information associated
// to that token
//
// It is recommended to use "mw" as import name of the package
// ex: mw "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustmiddleware"
package trustmiddleware

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/trustchile/publib/trust_utils_go/internal/cache"
	"gitlab.com/trustchile/publib/trust_utils_go/internal/logger"
	utils "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustutils"
)

// Structure used to decode the response from the oauth service when asking to
// verify an access token
type checkTokenResponse struct {
	ResourceOwnerID int      `json:"resource_owner_id"`
	Scope           []string `json:"scope"`
	ExpiresIn       int      `json:"expires_in"`
	Application     struct {
		UID        string `json:"uid"`
		CompanyUID string `json:"company_uid"`
	} `json:"application"`
	CreatedAt  int    `json:"created_at"`
	CompanyUID string `json:"company_uid"`
}

// Declaration of an alias to imported structures, to simplify the syntax of the
// code
type tokenInfo = utils.TokenInfo
type resourceOwner = utils.ResourceOwner

// Middleware function used to check and get info of an access token. The
// access token to check is retrieved from cache, if not found, try to get it
// from the request header. As all the middleware function, AuthCheck get a
// handler pointing to the next middleware or request as argument and return
// a handler indicating his operation
func AuthCheck(next http.Handler) http.Handler {

	// Method used to generate a new handler from a regular function
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// To start the function, is used the utility function NeedVerification to
		// determine if continue with the middleware. If the verification is not
		// needed, set empty structures in the context and finalize the middleware
		logger.Writter.Debug("Middleware AuthCheck")
		if !utils.NeedVerification() {

			// Set empty structures
			ctx := context.WithValue(r.Context(), utils.ROwnerKey, resourceOwner{})
			ctx = context.WithValue(ctx, utils.TInfoKey, tokenInfo{})

			// Function used to continue with the next middleware/request, after
			// updating the request with the new context generated
			next.ServeHTTP(w, r.WithContext(ctx))
			logger.Writter.Debug("End of Middleware AuthCheck")
			return
		}

		// If verification is needed, get the access token from the header of the
		// request and check that the token is not empty and that is a bearer token
		// type
		bearerToken := r.Header.Get("Authorization")
		bearerAuth := len(bearerToken) > 0 && strings.HasPrefix(bearerToken, "Bearer")

		// If some of the condition from before is not fulfilled, send a appropiated
		// response and stop the middleware flow
		// Error handling inspired by https://gist.github.com/elithrar/9146306
		if !bearerAuth {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, "Bearer token is required", http.StatusUnauthorized)
			logger.Writter.Debug("End of Middleware AuthCheck")
			return
		}

		// Define a variables pair to try to get the access token from cache, one is
		// used as pointer and the other as value, because create a empty struct
		// generate empty/null values in his fields, so dont point to nil and is
		// more difficult to verify if is empty
		var token *tokenInfo
		dummyToken := tokenInfo{}

		// Use the embedded cache to retrieve the token, and if in the process of
		// getting or unmarshaling the data some goes wrong, the pointer goes to nil
		bearerCache, found := cache.Data.Get(bearerToken)
		if found != nil {

			// The token is not found
			logger.Writter.Infow(fmt.Sprintf("Cache tokenInfo for %v not found.", bearerToken), "error", found)
			token = nil
		} else {
			err := json.Unmarshal(bearerCache, &dummyToken)

			// The token data could not be unmarshaled
			if err != nil {
				logger.Writter.Infow(fmt.Sprintf("Cache tokenInfo for %v cannot be decoded.", bearerToken), "error", err, "bearerCache", string(bearerCache))
				token = nil
			} else {

				// Everything is fine and the data is retrieved from cache
				token = &dummyToken
			}
		}

		// If the token info is not present in cache, send a request to oauth
		// service to check the token and retrieve the info
		if token == nil {
			var tokenErr error

			// Call the function in charge to execute the request
			token, tokenErr = checkAccessToken(bearerToken)

			if tokenErr == nil {

				// If the request is completed successfully, store the token info in
				// local cache
				if err := cache.Data.Set(bearerToken, token, time.Duration(5)*time.Minute); err != nil {
					logger.Writter.Infow("Problem storing token info in cache", "error", err)
				}
			} else {

				// If there is a problem trying to complete the request, send a
				// appropiated response and stop the middleware flow
				logger.Writter.Error("Problem while getting token: " + tokenErr.Error())
				w.Header().Set("Content-Type", "application/json")
				http.Error(w, "Problem while getting token: "+tokenErr.Error(), http.StatusUnprocessableEntity)
				return
			}
		}

		// To complete the token information, is determined if the token correspond
		// to a systemic token. First, is created a helper empty string
		tCompanyUID := ""

		// Then, try to retrieve the company uid from the context
		if ti := r.Context().Value(utils.TInfoKey); ti != nil {
			if tInfo, ok := ti.(tokenInfo); ok {
				tCompanyUID = tInfo.CompanyUID
			}
		}

		// If cannot retrieve the value, or equals to empty string, assign the value
		// of company uid present in the response of oauth provider
		if tCompanyUID == "" {
			tCompanyUID = token.CompanyUID
		}

		// If token company uid is "0", then is a systemic token
		token.SystemicToken = (tCompanyUID == "0")

		// Store the retrieved/new/updated token info in the request context
		ctx := context.WithValue(r.Context(), utils.TInfoKey, *token)

		// And continue with the next middleware/request
		next.ServeHTTP(w, r.WithContext(ctx))
		logger.Writter.Debug("End of Middleware AuthCheck")
	})
}

// Method used to wrap the request to the oauth service, his function is to
// verify the bearer token and to translate the oauth response into a tokenInfo
// structure. As argument, this function need the string token itself and as
// result, the method return a pointer to a structure with the token info
// obtained or a pointer to nil and a descriptive error
func checkAccessToken(bearerToken string) (*tokenInfo, error) {

	// If the bearer token is a empty string, stop the function
	if bearerToken == "" {
		logger.Writter.Info("Access token is missing from requests header")
		return nil, fmt.Errorf("access token is missing from requests header")
	}

	// Then call the request function
	ateneaResponse, ateneaErr := checkTokenInAteneaService(bearerToken)

	// If some fail in the process, return the error retrieved
	if ateneaErr != nil {
		return nil, ateneaErr
	}

	// If all is ok, copy the info obtained into a tokenInfo structure
	token := &tokenInfo{}

	token.CompanyUID = ateneaResponse.CompanyUID
	token.ResourceOwnerID = strconv.Itoa(ateneaResponse.ResourceOwnerID)
	token.Scopes = ateneaResponse.Scope
	token.ClientID = ateneaResponse.Application.UID

	// Then return a pointer to the structure
	return token, nil
}

// Method used to efectuate a request to the oauth service to check/get the info
// from an access token. As argument, this function need the string token itself
// As result, the method return a pointer to a structure with the response info
// obtained or a pointer to nil and a descriptive error
func checkTokenInAteneaService(bearerToken string) (*checkTokenResponse, error) {

	// First check if the required env variables are present
	providerURL := os.Getenv("OAUTH_PROVIDER_URL")

	if len(providerURL) == 0 {
		logger.Writter.Info("OAUTH_PROVIDER_URL ENV is missing")
		return nil, fmt.Errorf("OAUTH_PROVIDER_URL ENV is missing")
	}

	// Then, set up the structures to use to make the request
	url := utils.CleanHttpUrl(providerURL + "/oauth/token/info")

	method := "GET"
	client := &http.Client{}
	header := &http.Header{}

	// With the url, the method and a empty body, build the request
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		//sentry.CaptureException(err)
		logger.Writter.Errorw("Problem creating connection to Oauth Provider", "error", err)
		return nil, fmt.Errorf("problem creating connection to Oauth Provider: %v", err)
	}

	// Then, set the required header data, in this case, the bearer token
	header.Set("Authorization", bearerToken)
	req.Header = *header

	// Then execute the request
	response, err := client.Do(req)
	if err != nil {
		//sentry.CaptureException(err)
		logger.Writter.Errorw("Cant connect to Oauth Provider", "error", err)
		return nil, fmt.Errorf("cant connect to Oauth Provider: %v", err)
	}
	defer response.Body.Close()

	// Try to decode the body of the response in a check token Response structure
	ateneaResponse := &checkTokenResponse{}
	err = json.NewDecoder(response.Body).Decode(ateneaResponse)
	if err != nil {
		//sentry.CaptureException(err)
		logger.Writter.Errorw("Cant parse Atenea Response", "error", err)
		return nil, fmt.Errorf("cant parse Atenea Response: %v", err)
	}

	if response.StatusCode == http.StatusOK {

		// If the response is a status ok, return the token response structure to the
		// caller of the function
		return ateneaResponse, nil

	} else {

		// If the requests response is not 200, return an error using the status
		// code of the response and his body
		error_string := "Oauth Provider Server Error Response - " +
			"Status: " + response.Status + ". Body: "

		body, err := json.Marshal(ateneaResponse)
		if err != nil {
			error_string += fmt.Sprintf("Problem while marshal response body - %v", err)
		} else {
			error_string += string(body)
		}
		logger.Writter.Info(error_string)
		return nil, fmt.Errorf(strings.ToLower(error_string))
	}
}
