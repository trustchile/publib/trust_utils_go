// Package trustmiddleware implements, as the name says, differents middleware
// related to the trust services to adapt the different request associated to
// the services.
//
// The file tracecapture implements the header capture middleware, that used to
// retrieve custom header values, from now trace header values, and added to the
// request context
//
// It is recommended to use "mw" as import name of the package
// ex: mw "gitlab.com/trustchile/publib/trust_utils_go/pkg/trustmiddleware"
package trustmiddleware

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/trustchile/publib/trust_utils_go/internal/logger"
	trace "gitlab.com/trustchile/publib/trust_utils_go/pkg/tracehelper"
)

// Middleware function used to get the trace headers an store it in the context.
// As all the middleware function, AuthCheck get a handler pointing to the next
// middleware or request as argument and return a handler indicating his
// operation
func TraceCapture(next http.Handler) http.Handler {
	// Method used to generate a new handler from a regular function
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		/*
			An interesting details about the handling of header is that go assumes the
			headers key are in MIME style, i.e. the start letter of each word is in
			upper case, and because of that, convert all the incoming keys to MIME,
			overriding keys with same text different capitalization, i.e. if we had in
			the header this pair:
			{ content-type: text/html, content-Type: multipart/form-data} in that
			order, only the last one will be store and with his key as "Content-Type".

			This is also true when setting headers using the function Set, so if you
			want to store a header key with a different capitalization, retrieve the
			http.Header structure and save the key/value using the map syntax:
			header[key] = value
		*/

		// To start the function, create a header map structure to store the trace
		// header key/values captured from the header
		logger.Writter.Debug("Middleware TraceCapture")
		traceHeader := make(trace.HeaderMap)

		// Then, go over the posibles trace header keys, obtained from the trace
		// helper package, and check if is present in the actual request header
		for _, headerKey := range trace.GetHeaderKeys() {

			// Access to the header value
			if envValue := r.Header.Values(headerKey); envValue != nil {

				// If the trace header has a value, store in the header map
				traceHeader[headerKey] = envValue

				// Debug logger
				logger.Writter.Debugw("Get trace header", "theader", headerKey,
					"value", strings.Join(envValue[:], ","))
			}
		}

		// Store the retrieved trace headers info in the request context
		ctx := context.WithValue(r.Context(), trace.THeaderKey, traceHeader)

		// And continue with the next middleware/request
		next.ServeHTTP(w, r.WithContext(ctx))
		logger.Writter.Debug("End of Middleware TraceCapture")
	})
}
