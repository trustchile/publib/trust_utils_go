// Package slicehelper add some functions to operate slices of string with ease.
//
// It is recommended to use "slice" as import name of the package
// ex: slice "example.com/trust-package-go/internal/slicehelper"
//
// This is an internal package, so is expected to not be imported from another
// packages
package slicehelper

// Function substrat recreate the set substraction operation over two slices of
// strings, thats it, the diff slice equals to the a slice minus the elements
// that are present in the a and the b slice. (diff = a - b)
// Inspired by https://siongui.github.io/2018/03/14/go-set-difference-of-two-arrays/
func Substract(minuend []string, subtracting []string) (difference []string) {

	// First create a map using every substracting element as key and true as
	// value
	subMap := make(map[string]bool)
	for _, item := range subtracting {
		subMap[item] = true
	}

	// Then go over all the elements in the minuend,
	for _, item := range minuend {

		// Check if the element can be used as key in the substracting map
		if _, ok := subMap[item]; !ok {

			// If is not a key, add the element to the difference slice
			difference = append(difference, item)
		}
	}

	//Finally return the difference slice
	return
}

// Simple method used to verify if a string is present in a slice of string
// https://golangcode.com/check-if-element-exists-in-slice/
func HasValue(slice []string, value string) bool {

	// Go over the slice
	for _, item := range slice {

		// Check if the value equals to the selected element
		if item == value {

			// If it does, return true
			return true
		}
	}

	// If none of the elements equals, return false
	return false
}
