// Package logger operates as a kind of interface to the zap package, that wraps
// an instance of the zap SugaredLogger as a package variable, centralizing his
// operation and simplifying his syntax.
//
// This is an internal package, so is expected to not be imported from another
// packages
package logger

import (
	"fmt"
	"os"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Writter is the instance of zap SugaredLogger, used to allow the access of the
// resource globally
var Writter zap.SugaredLogger

// CreateLoggerInstance create a instance of the sugared logger and assign it to
// the Writer variable. Must be executed before trying to access to the Writer
// variable
func CreateLoggerInstance() {

	// Get a default configuration structure for the logger, then change the
	// encoding of the logs to json format
	ndc := zap.NewDevelopmentConfig()
	ndc.Encoding = "json"

	// Then, determine the logger level using environment variable
	env := strings.ToLower(os.Getenv("APP_ENV"))
	if env == "production" {
		ndc.Level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	} else {
		ndc.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	}

	// Get a structure with production configuration for the logs message encoder
	npec := zap.NewProductionEncoderConfig()

	// Then change the format for the display of the  datetime information.
	// Check https://yourbasic.org/golang/format-parse-string-time-date-example/
	// for datetime layout options
	npec.EncodeTime = zapcore.TimeEncoderOfLayout("02/01/2006 15:04:05.000")

	// After editing the message encoder, assign it to the configuration structure
	ndc.EncoderConfig = npec

	// Finally, generate a logger with this custom configuration
	zapLog, err := ndc.Build()
	if err != nil {
		panic(fmt.Sprintf("Fatal error on CreateLoggerInstance: (%v)?", err))
	}
	defer zapLog.Sync()

	// Then select the logger algorithm and assign the resulting logger to Writer
	// variable
	Writter = *zapLog.Sugar()
}
