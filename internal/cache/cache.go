// Package cache operates as a kind of interface to the go-cache pacakge, that
// convert all the incoming/outcoming data to byte array in the set/get method
//
// This is an internal package, so is expected to not be imported from another
// packages
package cache

import (
	"encoding/json"
	"fmt"
	"time"

	gocache "github.com/patrickmn/go-cache"
)

// Structure to mask the go-cache structure, to implement the new set and get
// methods
type AppCache struct {
	// Inspired by https://adityarama1210.medium.com/fast-golang-api-performance-with-in-memory-key-value-storing-cache-1b248c182bdb
	client *gocache.Cache
}

// Variable used to access to the cache as a package structure, simplifying the
// syntax for the call of the set/get methods
var Data *AppCache

// Function that inicialize the go-cache package structure. defaultExpiration
// determine the duration of the data inside the cache, cleanupInterval set the
// time to pass to make a clean of the cache structure.
// Check https://github.com/patrickmn/go-cache/blob/master/cache.go New function
// for more details of the arguments.
func CreateCacheInstance(defaultExpiration time.Duration, cleanupInterval time.Duration) {

	// Call for the GoCache New method and the resultant cache assign it to the
	// data variable
	Data = &AppCache{client: gocache.New(defaultExpiration, cleanupInterval)}
}

// Function that get the data associated to the key string as a byte array.
// Return the byte data and a descriptive error if data cannot be retrieved.
func (cache *AppCache) Get(key string) ([]byte, error) {

	// Use the go-cache get method to retrieve data from cache
	res, exist := cache.client.Get(key)

	// If data is not in the cache, return nil and an associated error
	if !exist {
		return nil, fmt.Errorf("given key dont have a asocciated value")
	}

	// Convert data to array of bytes
	resByte, ok := res.([]byte)

	// If convertion fails, return nil and an associated error
	if !ok {
		return nil, fmt.Errorf("format is not arr of bytes")
	}

	// Everything is fine, return data and a nil error
	return resByte, nil
}

// Function that store the argument data in the cache and associated him the key
// string. The data is stored as byte array. Return an error if the data cannot
// be converted to a byte array.
func (cache *AppCache) Set(key string, data interface{}, expiration time.Duration) error {

	// Using json package, convert the data to an jsonify structure in a byte
	// array
	dataByte, err := json.Marshal(data)

	// If cannot convert, return the error
	if err != nil {
		return err
	}

	// Use the go-cache set method to store the data in cache and return nil
	cache.client.Set(key, dataByte, expiration)
	return nil
}
